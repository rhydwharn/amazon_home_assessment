Feature: Test Amazon website

    As a user, I am able to access Amazon.com then I am able to perform some series of actions

    Scenario: Access Tablet Accessories page

    Given I am able to access amazon.com website
    And I click on "hamburger menu"
    And I click on "Computers"
    And I click on "Tablet Accessroies"
    And I insert "brandname" so as to filter the brand by "JETech"
    And I click on "search"
    And I sort the items by "Neweset Arrivals"
    And I click on the lowest item
    And I validate that "About this item" section exists
