import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'


let homeMenu
let searchBox

before(() => {

	//This section is to select items on the page.
	cy.fixture('selectors').then(sel => {
		//Instantiate the items that are on the selectors page
		homeMenu = sel.homepageMenu
		searchBox = sel.tabletAccessoriesPage

	})

})

Given(/^I am able to access amazon.com website$/, () => {
	//Access amazon website
	cy.visit('/')
	//Due to reload happening on the page, we had to intercept this url to stop the reload and solve flakkiness
	cy.intercept('/?ref=nav_em_linktree_fail').as('pageReload')
});

Then(/^I click on "([^"]*)"$/, (args1) => {
	switch (args1) {
		// Since the step is reusable, we are using a switch statement to dynamically pick the items
		case 'hamburger menu':
			//Click on the menu button
			cy.clickSpecifiedElement(homeMenu.hamburgerMainMenu).wait(2000)
			break
		case 'Computers':
			//Click on the computers menu item
			cy.clickSpecifiedElement(homeMenu.computersMenuItem)
			// cy.wait('@pageReload')
			break
		case 'Tablet Accessroies':
			//Click on tablet accessories
			cy.clickSpecifiedElement(homeMenu.tabletAccessoriesItem)
			break
		case 'search':
			//Click on search button
			cy.clickSpecifiedElement(searchBox.searchButton)
	}
});


Then(/^I insert "([^"]*)" so as to filter the brand by "([^"]*)"$/, (args1, args2) => {
	//Input JETech into the search input box
	args1 = searchBox.searchInput
	cy.typeDetails(args1, args2)
});


When(/^I sort the items by "([^"]*)"$/, (args1) => {
	//Sort items by value [Newest arrivals]
	console.log(args1);
	return true;
});

When(/^I click on the lowest item$/, () => {
	const { _ } = Cypress

	cy.get('[class="a-price"]').then(($prices) => {
		const text = Cypress._.map($prices, (p) => p.innerHTML)
		const firstWord = (text) => text.split()[0]
		//Getting the content from and splitting it into an array block
		const words = text.map(firstWord)
		const justDigits = (str) => str.replace(/[^0-9. ]/g, '')
		const almostNumbers = words.map(justDigits)
		const numbers = almostNumbers.map(parseFloat)
		const sorted = Cypress._.sortBy(numbers)

		const [firstItem, ...rest] = sorted;
		//Click the first amount minimum amount [if minimum amount appears more than once]
		cy.contains(firstItem).click()
	})
});

When(/^I validate that "([^"]*)" section exists$/, (args1) => {
	cy.getText(args1)
	//Validate that the about this item exist
	cy.get('#feature-bullets').then(($show) => {
		const text = Cypress._.map($show, (p) => p.innerText)
		//Log the text in the about item section
		cy.log(text)
	})


});