let data
before(() => {

    cy.fixture('selectors').then((sel) => {
        data = sel
    })
})

//Reusable code for clicking an element
Cypress.Commands.add('clickSpecifiedElement', (element) => {
    cy.get(element).should('be.visible').click({ multiple: true, force: true })
})

//Reusable code to type a text
Cypress.Commands.add('typeDetails', (field, text) => {
    cy.get(field).should('exist').type(text)
})

//Reusable get text 
Cypress.Commands.add('getText', (text) => {
    cy.contains(text).should('be.visible')
})