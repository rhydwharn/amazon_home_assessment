const { defineConfig } = require("cypress");
const preprocessor = require("@badeball/cypress-cucumber-preprocessor");
const browserify = require("@badeball/cypress-cucumber-preprocessor/browserify");

async function setupNodeEvents(on, config) {
  await preprocessor.addCucumberPreprocessorPlugin(on, config);

  on('file:preprocessor', browserify.default(config));

  return config

}

module.exports = defineConfig({
  e2e: {
    baseUrl: 'https://www.amazon.com',
    viewportHeight: 1080,
    viewportWidth: 1680,
    defaultCommandTimeout: 10000,
    chromeWebSecurity: false,
    watchForFileChanges: false,
    experimentalRunAllSpecs: true,
    specPattern:'**/*.feature',
    setupNodeEvents,
  },
});
