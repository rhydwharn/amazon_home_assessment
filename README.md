## Assessment was done using Cypress 

## DISCLAIMER!!!
It is worthy of note that amazon.com website is a flakky site and its unstable as different menus and display is been returned based on different browser type. The best experience for this test is on a userPC. However, the Assessment is linked to browserstack to run the test on multiple browsers. Also there is no switch page on the site opening on a new tab. Hence, the step 8 can not be achieved. Also it is observed that the sort by "New arrivals is not visible at all times, so the Assessment is using "

## To use this framework, the user need to clone this project and install cypress. The steps highlighted below will help install cypress and all necessary dependencies

## It is under the assumption that the user who wants to clone this project has set up node on his/her system
## If the user has not installed Node prior to now, the user should get node using the link below;
https://nodejs.org/en/download

## After the node has been installed, the user need to have an IDE installed, I will recommend VS Code, this can be installed using the link below
https://code.visualstudio.com/download

## After the VS code and the node has been installed, the user needs to navigate to the folder that the project was cloned into, then launch terminal as we need to install some other dependencies for the project to run. The following codes should be run on the terminal one after the other.

## Create a folder where the project will be cloned to then proceed to initiate GIT and follow other steps. Go to Terminal and click on new terminal

## Initialize GIT
git init 

## Clone the project
git clone https://gitlab.com/rhydwharn/amazon_home_assessment;    

## After stating the location for the origin, the user need to set up other dependencies. To have a smooth running of the installations, the parent folder that the terminal must be run is amazon_home_assessment folder
npm i

## The above installs all dependencies, now the user can run the cypress using the command below
npx cypress open --e2e

## To run the test on browserstack, the user needs to register for a browserstack account on https://browserstack.com after the registration, the user needs to activate the account then proceed to the dashboard. From the browserstack dashboard, the user click on profile/account, click on Profile then copy the username to replace the BROWSERSTACK_USERNAME, password to replace the BROWSERSTACK_ACCESS_KEY that exist on the browserstack.json file

## If the user decides to run the code via browserstack directly, the code below will launch the code via browserstack and the user can monitor the deployment via browserstack
browserstack-cypress run   
